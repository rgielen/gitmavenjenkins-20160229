package net.rgielen.gitmavenjenkins;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloWorldTest {

    @Test
    public void testHelloWorldClassCanBeInstantiated() throws Exception {
        assertNotNull(new HelloWorld());
    }

    @Test
    public void testGetHelloWorldMessageDeliversHello() throws Exception {
        assertEquals("Hello", new HelloWorld().getHelloMessage());
    }

}
