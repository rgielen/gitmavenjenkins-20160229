package net.rgielen.gitmavenjenkins;

import org.apache.log4j.Logger;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloWorld {

    private static Logger LOG = Logger.getLogger(HelloWorld.class);

    public static void main(String[] args) {
        LOG.info(new HelloWorld().getHelloMessage());
    }

    String getHelloMessage() {
        return "Hello";
    }

    public void someUntestedMethod() {
        System.out.println("untested ...");
    }

}
